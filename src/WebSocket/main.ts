import { io } from "socket.io-client";
import { reactive, computed } from "vue";
import {
  allBoards,
  addBoard,
  removeBoard,
  addMemberToBoardWS,
  removeMemberFromBoardWS,
} from "@/store/board";
import { addList, removeList, editListName } from "@/store/list";
import {
  allCards,
  addCard,
  removeCard,
  editCard,
  MemberAddedToCardWS,
  MemberRemovedFromCardWS,
  addTodoWS,
  removeTodoWS,
  editTodoNameWS,
  checkTodoWS,
  unCheckTodoWS,
} from "@/store/card";
import { user } from "@/store/user";
import { emitter } from "../mitt";
// import { user } from "@/store/user";

let socket: any;

let data = reactive({
  onlineUsers: {} as any,
});

// Init WebSocket
export const initializSocket = () => {
  openSocket();
  showNotification("شما با WebSocket به سرور متصل شدید.");

  // USERS ACTIONS
  socket.on("updateUserRoom", buildOnlineUsers);

  // Sending users info and user boards
  socket.emit("Initial", { user: user.value, allBoards: allBoards.value });

  // BOARD ACTIONS
  // Someone added a board which this user is one of it's members
  socket.on("boardAdded", boardAdded);

  // A board which this user is one of its members has been deleted
  socket.on("boardRemoved", boardRemoved);

  // Someone has been added to the board which this user is one of it's users
  socket.on("someoneAddedToBoard", someoneAddedToBoard);

  // this user has been added to a board
  socket.on("youHaveBeenAddedToBoard", youHaveBeenAddedToBoard);

  // Someone has been removed from a board which this user is one of it's users
  socket.on("someoneRemovedFromBoard", someoneRemovedFromBoard);

  // this user has been removed from a board
  socket.on("youHaveBeenRemovedFromBoard", youHaveBeenRemovedFromBoard);

  // LIST ACTIONS
  socket.on("listAdded", listAdded);

  socket.on("listRemoved", listRemoved);

  socket.on("listEditted", listEditted);

  // CARD ACTIONS
  socket.on("cardAdded", cardAdded);

  socket.on("cardRemoved", cardRemoved);

  socket.on("cardEditted", cardEditted);

  socket.on("memberAddedToCard", memberAddedToCard);

  socket.on("memberRemovedFromCard", memberRemovedFromCard);

  socket.on("TodoEditted", TodoEditted);

  socket.on("TodoAdded", TodoAdded);

  socket.on("TodoRemoved", TodoRemoved);

  socket.on("TodoUnchecked", TodoUnchecked);

  socket.on("TodoChecked", TodoChecked);

  socket.on("message", (s: any) => {
    console.log(s);
  });
};

const buildOnlineUsers = (users: Array<any>) => {
  console.log("got online users list: ");
  console.log(users);
  data.onlineUsers = users;
};

export const isUserOnline = (id: string) => {

  let isUserOnline = false;

  data.onlineUsers.users.forEach((user: any) => {
    if (user.id === id) isUserOnline = true;
  });

  return isUserOnline;
};

// Board Actions
export const addBoardForAll = (board: any) => {
  console.log("emitting newBoard");
  socket.emit("newBoard", { board });
};

const boardAdded = ({ board, username }: any) => {
  addBoard(board, false);
  const text = `بورد <b>${board.name}</b> توسط <b class="text-secondary-300">${username}</b> ایجاد شد.`;
  showNotification(text);
};

export const removeBoardForAll = (board_id: string) => {
  socket.emit("removeBoard", { board_id });
};

const boardRemoved = ({ board_id, username }: any) => {
  emitter.emit("boardRemoved", { board_id });
  const removedBoard = removeBoard(board_id, false);
  const text = `بورد <b>${removedBoard.name}</b> توسط <b class="text-secondary-300">${username}</b> حذف شد.`;
  showNotification(text);
};

export const addedSomeoneToBoard = (board_id: string, member: any) => {
  socket.emit("addSomeoneToBoard", { board_id, member });
};

export const removedSomeoneFromBoard = (board_id: string, member: any) => {
  socket.emit("removedSomeoneFromBoard", { board_id, member });
};

const someoneAddedToBoard = ({ member, boardName }: any) => {
  addMemberToBoardWS(boardName, member);
  const text = `<b>${member.username}</b> به اعضای بورد <b class="text-secondary-300">${boardName}</b> اضافه شد.`;
  showNotification(text);
};

const youHaveBeenAddedToBoard = ({ board }: any) => {
  if (!user.value.admin) {
    addBoard(board, false);
  }
  const text = `<b>شما</b> به اعضای بورد <b class="text-secondary-300">${board.name}</b> اضافه شدید.`;
  showNotification(text);
};

const someoneRemovedFromBoard = ({ member, boardName }: any) => {
  removeMemberFromBoardWS(boardName, member);
  const text = `<b>${member.username}</b> از اعضای بورد <b class="text-secondary-300">${boardName}</b> حذف شد.`;
  showNotification(text);
};

const youHaveBeenRemovedFromBoard = ({ board_id }: any) => {
  emitter.emit("boardRemoved", { board_id });
  const removedBoard = removeBoard(board_id, false);
  const text = `<b>شما</b> از اعضای بورد <b class="text-secondary-300">${removedBoard.name}</b> حذف شدید.`;
  showNotification(text);
};

// LIST ACTIONS
// Emitters
export const addListForAll = (board_id: string, list: any) => {
  socket.emit("newList", { board_id, list });
};

export const removeListForAll = (board_id: string, list_id: string) => {
  socket.emit("removeList", { board_id, list_id });
};

export const editListForAll = (
  board_id: string,
  list_id: string,
  newName: string
) => {
  socket.emit("editList", { board_id, list_id, newName });
};

// Listeners
const listAdded = ({ list, username }: any) => {
  addList(list, false);
  const text = `لیست <b>${list.name}</b> توسط <b class="text-secondary-300">${username}</b> ایجاد شد.`;
  showNotification(text);
};

const listRemoved = ({ list_id, username }: any) => {
  emitter.emit("listRemoved", { list_id });
  const removedList = removeList(list_id, false);
  if (removedList === false) return;
  const text = `لیست <b>${removedList.name}</b> توسط <b class="text-secondary-300">${username}</b> حذف شد.`;
  showNotification(text);
};

const listEditted = ({ list_id, newName, username }: any) => {
  console.log(list_id);
  console.log(newName);
  const edittedCard = editListName(list_id, newName, false);
  if (edittedCard === false) return;
  const text = `نام لیست <b>${edittedCard}</b> توسط <b class="text-secondary-300">${username}</b> به <b>${newName}</b> تغییر پیدا کرد..`;
  showNotification(text);
};

// CARD ACTIONS
export const addCardForAll = (board_id: string, card: any) => {
  socket.emit("newCard", { board_id, card });
};

export const removeCardForAll = (board_id: string, card_id: string) => {
  socket.emit("removeCard", { board_id, card_id });
};

export const editCardForAll = (board_id: string, newCard: any) => {
  socket.emit("editCard", { board_id, newCard });
};

const cardAdded = ({ card, username }: any) => {
  if (addCard(card, false) === false) return;
  const text = `کارت <b>${card.name}</b> توسط <b class="text-secondary-300">${username}</b> ایجاد شد.`;
  showNotification(text);
};

const cardRemoved = ({ card_id, username }: any) => {
  emitter.emit("cardRemoved", { card_id });
  const removedCard = removeCard(card_id, false);
  const text = `کارت <b>${removedCard.name}</b> توسط <b class="text-secondary-300">${username}</b> حذف شد.`;
  showNotification(text);
};

const cardEditted = ({ newCard, username }: any) => {
  const edittedCard = editCard(newCard, false);
  if (edittedCard === false) return;
  const text = `کارت <b>${edittedCard.name}</b> توسط <b class="text-secondary-300">${username}</b> ویرایش شد.`;
  showNotification(text);
};

export const addedMemberToCard = (
  board_id: string,
  member: any,
  card_id: string
) => {
  socket.emit("addMemberToCard", { board_id, member, card_id });
};

export const removedMemberFromCard = (
  board_id: string,
  member_id: any,
  card_id: string
) => {
  socket.emit("removedMemberFromCard", { board_id, member_id, card_id });
};

const memberAddedToCard = ({ card_id, member }: any) => {
  // action
  if (MemberAddedToCardWS(card_id, member) === false) return;

  const cardIdx = allCards.value.findIndex((card) => card._id === card_id);
  const cardName = allCards.value[cardIdx].name;

  let text;
  if (member._id === user.value.id) {
    text = `<b>شما</b> به اعضای کارت <b class="text-secondary-300">${cardName}</b> اضافه شدید.`;
  } else {
    text = `<b>${member.username}</b> به اعضای کارت <b class="text-secondary-300">${cardName}</b> اضافه شد.`;
  }
  showNotification(text);
};

const memberRemovedFromCard = ({ card_id, member_id }: any) => {
  const removedMember = MemberRemovedFromCardWS(card_id, member_id);

  if (removedMember === false) return;

  const cardIdx = allCards.value.findIndex((card) => card._id === card_id);
  const cardName = allCards.value[cardIdx].name;

  let text;
  if (member_id === user.value.id) {
    text = `<b>شما</b> از اعضای کارت <b class="text-secondary-300">${cardName}</b> حذف شدید.`;
  } else {
    text = `<b>${removedMember.username}</b> از اعضای کارت <b class="text-secondary-300">${cardName}</b> حذف شد.`;
  }
  showNotification(text);
};

export const EditTodo = (
  board_id: string,
  card_id: string,
  index: number,
  todo_name: any
) => {
  socket.emit("EditTodo", { board_id, card_id, index, todo_name });
};

export const AddTodo = (board_id: string, card_id: string, todo: any) => {
  socket.emit("AddTodo", { board_id, card_id, todo });
};

export const RemoveTodo = (
  board_id: string,
  card_id: string,
  index: number
) => {
  socket.emit("RemoveTodo", { board_id, card_id, index });
};

export const CheckTodo = (board_id: string, card_id: string, index: number) => {
  socket.emit("CheckTodo", { board_id, card_id, index });
};

export const UncheckTodo = (
  board_id: string,
  card_id: string,
  index: number
) => {
  socket.emit("UncheckTodo", { board_id, card_id, index });
};

export const AddCardAttatchment = (
  board_id: string,
  card_id: string,
  attach: string
) => {
  socket.emit("AddCardAttatchment", { board_id, card_id, attach });
};

export const RemoveCardAttatchment = (
  board_id: string,
  card_id: string,
  index: number
) => {
  socket.emit("RemoveCardAttatchment", { board_id, card_id, index });
};

const TodoEditted = ({ board_id, card_id, index, newName, username }: any) => {
  const oldName = editTodoNameWS(index, newName, card_id);
  if (oldName === false) return;

  const board = allBoards.value.find((board) => board._id === board_id);
  const card = allCards.value.find((card) => card._id === card_id);
  const text = `نام تسک <b>${oldName}</b> از کارت <b>${card.name}</b> در بورد </b>${board.name}<b> توسط <b class="text-secondary-300">${username}</b> به <b>${newName}</b> تغییر کرد.`;
  showNotification(text);
};

const TodoAdded = ({ board_id, card_id, todo, username }: any) => {
  if (addTodoWS(card_id, todo.name) === false) return;

  const board = allBoards.value.find((board) => board._id === board_id);
  const card = allCards.value.find((card) => card._id === card_id);
  const text = `تسک <b>${todo.name}</b> از کارت <b>${card.name}</b> در بورد <b>${board.name}<b/> توسط <b class="text-secondary-300">${username}</b> اضافه شد.`;
  showNotification(text);
};

const TodoRemoved = ({ board_id, card_id, index, username }: any) => {
  const todo = removeTodoWS(index, card_id);
  if (todo === false) return;

  const board = allBoards.value.find((board) => board._id === board_id);
  const card = allCards.value.find((card) => card._id === card_id);
  const text = `تسک <b>${todo.name}</b> از کارت <b>${card.name}</b> در بورد <b>${board.name}</b> توسط <b class="text-secondary-300">${username}</b> حذف شد.`;
  showNotification(text);
};

const TodoChecked = ({ board_id, card_id, index, username }: any) => {
  const todo = checkTodoWS(card_id, index);
  if (todo === false) return;

  const board = allBoards.value.find((board) => board._id === board_id);
  const card = allCards.value.find((card) => card._id === card_id);
  const text = `تسک <b>${todo.name}</b> از کارت <b>${card.name}</b> در بورد <b>${board.name}</b> توسط <b class="text-secondary-300">${username}</b> انجام شد.`;
  showNotification(text);
};

const TodoUnchecked = ({ board_id, card_id, index, username }: any) => {
  const todo = unCheckTodoWS(card_id, index);
  if (todo === false) return;

  const board = allBoards.value.find((board) => board._id === board_id);
  const card = allCards.value.find((card) => card._id === card_id);
  const text = `تسک <b>${todo.name}</b> از کارت <b>${card.name}</b> در بورد <b>${board.name}</b> توسط <b class="text-secondary-300">${username}</b> خالی شد.`;
  showNotification(text);
};

const showNotification = (notification: string) => {
  emitter.emit("showNotification", {
    text: notification,
    kind: "info",
  });
};

export const openSocket = () => {
  if (process.env.NODE_ENV === "development") {
    try {
      socket = io();
      console.log("connection oppened on development");
    } catch (err) {
      console.log(err.message);
    }
  } else {
    try {
      socket = io();
      console.log("connection oppened on production");
    } catch (err) {
      console.log(err.message);
    }
  }
};

export const closeSocket = () => {
  if (socket) {
    socket.close();
  }
};

export const onlineUsers = computed(() => data.onlineUsers);
