import { reactive, computed } from "vue";
import { activeBoard } from "./board";
import { getBoardId } from "./list";
import {
  addCardForAll,
  removeCardForAll,
  editCardForAll,
  addedMemberToCard,
  removedMemberFromCard,
  EditTodo,
  AddTodo,
  RemoveTodo,
  CheckTodo,
  UncheckTodo,
  AddCardAttatchment,
  RemoveCardAttatchment,
} from "@/WebSocket/main";

const Card = reactive({
  AllCards: [] as Array<any>,
  ActiveCard: {
    id: "",
    name: "",
    todos: [],
    tags: [],
    attachments: [],
    color: "",
    members: [],
    list_id: "",
    owner: "",
  },
});

export const buildActiveCard = (data: any) => {
  data = JSON.parse(JSON.stringify(data));
  Card.ActiveCard.id = data._id || data.id || "";
  Card.ActiveCard.name = data.name || "";
  Card.ActiveCard.todos = data.todos || [];
  Card.ActiveCard.tags = data.tags || [];
  Card.ActiveCard.attachments = data.attachments || [];
  Card.ActiveCard.color = data.color || "";
  Card.ActiveCard.list_id = data.list_id || "";
  Card.ActiveCard.members = data.members || [];
  Card.ActiveCard.owner = data.owner || "";
};

export const emptyActiveCard = () => {
  Card.ActiveCard.id = "";
  Card.ActiveCard.name = "";
  Card.ActiveCard.todos = [];
  Card.ActiveCard.tags = [];
  Card.ActiveCard.attachments = [];
  Card.ActiveCard.color = "";
  Card.ActiveCard.list_id = "";
  Card.ActiveCard.members = [];
  Card.ActiveCard.owner = "";
};

export const removeBoardCards = (id: string) => {
  Card.AllCards.forEach((card, index) => {
    if (card.list_id === id) Card.AllCards.splice(index, 1);
  });
};

export const buildAllCards = (data: any) => {
  Card.AllCards = data;
};

export const emptyAllCards = () => {
  Card.AllCards = [];
};

export const addCard = (
  card: any,
  broadCast: Boolean = true,
  boardId: string = ""
) => {
  if (boardId.length > 0 && activeBoard.value.id !== boardId) return false;
  if (broadCast) addCardForAll(getBoardId(card.list_id), card);
  Card.AllCards.push(card);
};

export const removeCard = (id: string, broadCast: Boolean = true) => {
  const index = Card.AllCards.findIndex((card) => card._id === id);

  if (index) return false;

  const removedCard = Card.AllCards[index];

  Card.AllCards.splice(index, 1);

  if (broadCast)
    removeCardForAll(getBoardId(removedCard.list_id), removedCard._id);
  else {
    return removedCard;
  }
};

export const addMemberToCard = (member: any, broadCast: Boolean = true) => {
  const id = Card.ActiveCard.id;
  Card.ActiveCard.members.push(member);
  const index = Card.AllCards.findIndex((card) => card._id === id);
  const board_id = getBoardId(Card.AllCards[index].list_id);
  Card.AllCards[index].members.push(member);

  if (broadCast) addedMemberToCard(board_id, member, id);
};

export const removeMemberFromCard = (
  member: any,
  broadCast: Boolean = true
) => {
  const id = Card.ActiveCard.id;
  const memberIndex = Card.ActiveCard.members.findIndex(
    (member) => member._id === member._id
  );
  Card.ActiveCard.members.splice(memberIndex, 1);
  const index = Card.AllCards.findIndex((card) => card._id === id);
  const board_id = getBoardId(Card.AllCards[index].list_id);
  Card.AllCards[index].members.splice(memberIndex, 1);

  if (broadCast) removedMemberFromCard(board_id, member._id, id);
};

export const editCard = (newCard: any, broadCast: Boolean = true) => {
  const index = Card.AllCards.findIndex((card) => card._id === newCard.id);

  if (index === -1) return false;

  Card.AllCards[index].name = newCard.name;
  Card.AllCards[index].tags = newCard.tags;
  Card.AllCards[index].color = newCard.color;
  Card.ActiveCard.name = newCard.name;
  Card.ActiveCard.tags = newCard.tags;
  Card.ActiveCard.color = newCard.color;

  if (broadCast) {
    const board_id = getBoardId(Card.AllCards[index].list_id);
    const card_id = Card.AllCards[index]._id;
    editCardForAll(board_id, newCard);
  } else return Card.AllCards[index];
};

export const toggleTodoCard = (index: number) => {
  const cardIdx = Card.AllCards.findIndex(
    (card) => card._id === Card.ActiveCard.id
  );

  const board_id = getBoardId(Card.AllCards[cardIdx].list_id);
  const card_id = Card.AllCards[cardIdx]._id;

  if (Card.ActiveCard.todos[index].checked) {
    Card.ActiveCard.todos[index].checked = !Card.ActiveCard.todos[index]
      .checked;
    Card.AllCards[cardIdx].todos[index].checked = !Card.AllCards[cardIdx].todos[
      index
    ].checked;
    UncheckTodo(board_id, card_id, index);
  } else {
    Card.ActiveCard.todos[index].checked = !Card.ActiveCard.todos[index]
      .checked;
    Card.AllCards[cardIdx].todos[index].checked = !Card.AllCards[cardIdx].todos[
      index
    ].checked;
    CheckTodo(board_id, card_id, index);
  }
};

export const addTodo = (name: string, broadCast: Boolean = true) => {
  const cardIdx = Card.AllCards.findIndex(
    (card) => card._id === Card.ActiveCard.id
  );

  const todo = { name, checked: false };
  Card.AllCards[cardIdx].todos.push(todo);

  if (broadCast) {
    const board_id = getBoardId(Card.AllCards[cardIdx].list_id);
    const card_id = Card.AllCards[cardIdx]._id;
    AddTodo(board_id, card_id, todo);
  }

  if (
    Card.ActiveCard.id.length &&
    Card.AllCards[cardIdx]._id === Card.ActiveCard.id
  )
    Card.ActiveCard.todos.push(todo);
};

export const editTodoName = (
  index: number,
  name: string,
  broadCast: Boolean = true
) => {
  const cardIdx = Card.AllCards.findIndex(
    (card) => card._id === Card.ActiveCard.id
  );

  if (broadCast) {
    const board_id = getBoardId(Card.AllCards[cardIdx].list_id);
    const card_id = Card.AllCards[cardIdx]._id;
    EditTodo(board_id, card_id, index, name);
  }

  Card.AllCards[cardIdx].todos[index].name = name;
  if (
    Card.ActiveCard.id.length &&
    Card.AllCards[cardIdx]._id === Card.ActiveCard.id
  )
    Card.ActiveCard.todos[index].name = name;
};

export const removeTodo = (index: number, broadCast: Boolean = true) => {
  const cardIdx = Card.AllCards.findIndex(
    (card) => card._id === Card.ActiveCard.id
  );

  if (broadCast) {
    const board_id = getBoardId(Card.AllCards[cardIdx].list_id);
    const card_id = Card.AllCards[cardIdx]._id;
    RemoveTodo(board_id, card_id, index);
  }

  Card.AllCards[cardIdx].todos.splice(index, 1);

  if (
    Card.ActiveCard.id.length &&
    Card.AllCards[cardIdx]._id === Card.ActiveCard.id
  )
    Card.ActiveCard.todos.splice(index, 1);
};

export const addAttachment = (newAttach: string) => {
  console.log();
  const cardIdx = Card.AllCards.findIndex(
    (card) => card._id === Card.ActiveCard.id
  );

  Card.AllCards[cardIdx].attachments.push(newAttach);
  Card.ActiveCard.attachments.push(newAttach);
};

export const removeAttachment = (index: number) => {
  const cardIdx = Card.AllCards.findIndex(
    (card) => card._id === Card.ActiveCard.id
  );

  Card.AllCards[cardIdx].attachments.splice(index, 1);
  Card.ActiveCard.attachments.splice(index, 1);
};

// WEB SOCKET functions:
export const MemberAddedToCardWS = (card_id: string, member: any) => {
  const index = Card.AllCards.findIndex((card) => card._id === card_id);

  if (index === -1) return false;

  Card.AllCards[index].members.push(member);

  if (
    Card.ActiveCard.id.length &&
    Card.ActiveCard.id === Card.AllCards[index]._id
  ) {
    Card.ActiveCard.members.push(member);
  }
};

export const MemberRemovedFromCardWS = (card_id: string, member_id: any) => {
  const index = Card.AllCards.findIndex((card) => card._id === card_id);

  if (index === -1) return false;

  const memberIdx = Card.AllCards[index].members.findIndex(
    (currentMember: any) => currentMember._id === member_id
  );
  const removedMember = Card.AllCards[index].members[memberIdx];
  Card.AllCards[index].members.splice(memberIdx, 1);

  if (
    Card.ActiveCard.id.length &&
    Card.ActiveCard.id === Card.AllCards[index]._id
  ) {
    Card.ActiveCard.members.splice(memberIdx, 1);
  }
  return removedMember;
};

export const addTodoWS = (card_id: string, name: string) => {
  const cardIdx = Card.AllCards.findIndex((card) => card._id === card_id);

  if (cardIdx === -1) return false;

  const todo = { name, checked: false };
  Card.AllCards[cardIdx].todos.push(todo);

  if (
    Card.ActiveCard.id.length &&
    Card.AllCards[cardIdx]._id === Card.ActiveCard.id
  )
    Card.ActiveCard.todos.push(todo);
};

export const editTodoNameWS = (
  index: number,
  name: string,
  card_id: string
) => {
  const cardIdx = Card.AllCards.findIndex((card) => card._id === card_id);
  console.log("Card index: ", cardIdx);
  console.log("Todo index: ", index);

  if (cardIdx === -1) return false;

  const oldName = Card.AllCards[cardIdx].todos[index].name;
  Card.AllCards[cardIdx].todos[index].name = name;

  if (
    Card.ActiveCard.id.length &&
    Card.AllCards[cardIdx]._id === Card.ActiveCard.id
  )
    Card.ActiveCard.todos[index].name = name;

  return oldName;
};

export const removeTodoWS = (index: number, card_id: string) => {
  const cardIdx = Card.AllCards.findIndex((card) => card._id === card_id);

  if (cardIdx === -1) return false;

  const todo = Card.AllCards[cardIdx].todos[index];
  Card.AllCards[cardIdx].todos.splice(index, 1);

  if (
    Card.ActiveCard.id.length &&
    Card.AllCards[cardIdx]._id === Card.ActiveCard.id
  )
    Card.ActiveCard.todos.splice(index, 1);

  return todo;
};

export const checkTodoWS = (card_id: string, index: number) => {
  const cardIdx = Card.AllCards.findIndex((card) => card._id === card_id);

  if (cardIdx === -1) return false;

  Card.AllCards[cardIdx].todos[index].checked = true;

  if (
    Card.ActiveCard.id.length &&
    Card.AllCards[cardIdx]._id === Card.ActiveCard.id
  )
    Card.ActiveCard.todos[index].checked = true;

  return Card.AllCards[cardIdx].todos[index];
};

export const unCheckTodoWS = (card_id: string, index: number) => {
  const cardIdx = Card.AllCards.findIndex((card) => card._id === card_id);

  if (cardIdx === -1) return false;

  Card.AllCards[cardIdx].todos[index].checked = false;

  if (
    Card.ActiveCard.id.length &&
    Card.AllCards[cardIdx]._id === Card.ActiveCard.id
  )
    Card.ActiveCard.todos[index].checked = false;

  return Card.AllCards[cardIdx].todos[index];
};

export const activeCard = computed(() => Card.ActiveCard);
export const allCards = computed(() => Card.AllCards);
