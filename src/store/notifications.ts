import { reactive, computed } from "vue";

let Notifs = reactive({
  AllNotifications: [] as Array<any>,
  loading: false as Boolean,
});

export const loadingStart = () => (Notifs.loading = true);

export const buildNotif = (data: any) => {
  Notifs.AllNotifications = data || [];
  Notifs.loading = false;
};

export const emptyNotifs = () => {
  Notifs.AllNotifications = [];
};

export const seenNotif = (id: string) => {
  const index = Notifs.AllNotifications.findIndex((notif) => notif._id === id);
  console.log(Notifs.AllNotifications[index]);
  Notifs.AllNotifications[index].seen = true;
  console.log(Notifs.AllNotifications[index]);
};

export const addNotif = (newNotif: object) => {
  Notifs.AllNotifications.unshift(newNotif);
};

export const removeNotif = (id: string) => {
  const index = Notifs.AllNotifications.findIndex((notif) => notif._id === id);

  Notifs.AllNotifications.splice(index, 1);
};

export const notifications = computed(() => Notifs.AllNotifications);
export const loading = computed(() => Notifs.loading);
