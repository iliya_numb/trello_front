import { reactive, computed } from "vue";
import { allCards, removeBoardCards } from "./card";
import {
  addListForAll,
  removeListForAll,
  editListForAll,
} from "@/WebSocket/main";

const List = reactive({
  AllLists: [] as Array<any>,
  ActiveList: {
    id: "",
    name: "",
    owner: "",
    board_id: "",
  },
});

export const buildActiveList = (data: any) => {
  data = JSON.parse(JSON.stringify(data));
  List.ActiveList.id = data._id || data.id || "";
  List.ActiveList.name = data.name || "";
  List.ActiveList.owner = data.owner || "";
  List.ActiveList.board_id = data.board_id || "";
};

export const emptyActiveList = () => {
  List.ActiveList.id = "";
  List.ActiveList.name = "";
  List.ActiveList.owner = "";
};

export const removeBoardLists = (id: string) => {
  List.AllLists.forEach((list, index) => {
    if (list.board_id === id) {
      removeBoardCards(list._id);
      List.AllLists.splice(index, 1);
    }
  });
};

export const buildAllLists = (data: any) => {
  List.AllLists = data;
};

export const emptyAllLists = () => {
  List.AllLists = [];
};

export const addList = (list: any, broadCast: Boolean = true) => {
  List.AllLists.push(list);
  if (broadCast) addListForAll(list.board_id, list);
};

export const removeList = (id: string, broadCast: Boolean = true) => {
  const index = List.AllLists.findIndex((list) => list._id === id);
  const removedList = List.AllLists[index];

  List.AllLists.splice(index, 1);

  if (broadCast) removeListForAll(removedList.board_id, id);
  else return removedList;
};

export const editListName = (
  id: string,
  newName: string,
  broadCast: Boolean = true
) => {
  const index = List.AllLists.findIndex((list) => list._id === id);
  console.log(List.AllLists);
  console.log(index);

  if (index === -1) return false;

  const list = List.AllLists[index];
  const oldName = list.name;
  List.AllLists[index].name = newName;

  if (List.ActiveList.id.length && List.ActiveList.id === id)
    List.ActiveList.name = name;

  if (broadCast) editListForAll(list.board_id, id, newName);
  else return oldName;
};

export const getBoardId = (list_id: string) => {
  const listIdx = List.AllLists.findIndex((list) => list._id === list_id);
  return List.AllLists[listIdx].board_id;
};
export const activeList = computed(() => List.ActiveList);
export const allLists = computed(() => List.AllLists);
